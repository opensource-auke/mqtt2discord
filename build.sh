#!/usr/bin/env bash
# build the container and push it to the gitlab repository

app=mqtt2discord
version=1.0.3-dev

if [ $# -ne 1 ]
then
  echo "Need one arguments"
  echo "platform: gitlab docker all"
  read platform

else 
  platform=$1
fi

echo "Build and push version ($version) to platform ($platform)"

case $platform in
  docker)
  echo "Login to docker"
  docker login 1>/dev/null 2>/dev/null || {echo "Not logged in to docker" exit 1}
  echo "Pushing to docker"
  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v6,linux/arm/v7,linux/arm64/v8,linux/amd64 \
  --tag aukevanderheide/$app:$version \
  --tag aukevanderheide/$app:latest .
  ;;
  gitlab)
  echo "Login to gitlab"
  docker login registry.gitlab.com 1>/dev/null 2>/dev/null || {echo "Not logged in to gitlab" exit 1}
  echo "Pushing to gitlab"
  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
  --tag registry.gitlab.com/opensource-auke/mqtt2discord/$app:$version \
  --tag registry.gitlab.com/opensource-auke/mqtt2discord/$app:latest .
  ;;
  all)
  echo "Login to docker"
  docker login 1>/dev/null 2>/dev/null || {echo "Not logged in to docker" exit 1}
  echo "Login to gitlab"
  docker login registry.gitlab.com 1>/dev/null 2>/dev/null || {echo "Not logged in to gitlab" exit 1}
  echo "Pushing to gitlab and docker"
  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
  --tag aukevanderheide/$app:$version \
  --tag aukevanderheide/$app:latest \
  --tag registry.gitlab.com/opensource-auke/mqtt2discord/$app:$version \
  --tag registry.gitlab.com/opensource-auke/mqtt2discord/$app:latest .
  ;;
  *)
  echo "Unknown platform: Use docker or gitlab"
  ;;
esac
