FROM --platform=$BUILDPLATFORM python:3.9
# FROM python:3.9

ARG TARGETPLATFORM
ARG BUILDPLATFORM

LABEL org.opencontainers.image.authors=aukevanderheide@gmail.com
LABEL org.opencontainers.image.url=https://gitlab.com/dockerauke/swarm/container_registry/2960539
LABEL org.opencontainers.image.documentation=https://gitlab.com/opensource-auke/mqtt2discord/-/wikis/home
LABEL org.opencontainers.image.source=https://gitlab.com/opensource-auke/mqtt2discord/-/tree/main
LABEL org.opencontainers.image.version=1.0
LABEL org.opencontainers.image.revision=3
LABEL org.opencontainers.image.licenses='todo'
LABEL org.opencontainers.image.title=Mqtt2Discord
LABEL org.opencontainers.image.description='Listens to mqtt topics and send payload to discord channel'

WORKDIR /usr/src/bot

COPY ../src/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ../src/*.py .
COPY ../src/config.json .

CMD ["python", "./main.py"]

# Testing for Multi Platform
 RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /log