# Welcome to my [mqtt2discord](https://gitlab.com/opensource-auke/mqtt2discord/-/wikis/home) project.

## What's it for

This app listens to **mqtt** topics and sends the payload to a **discord** channel.

It can be used with multiple **mqtt** topics and multiple **discord** channels.

Because it uses *webhooks* for sending to **discord** there is no need to register a bot in **discord**.

## Status

**Current version [v1.0.0](%v1.0.0)**

[Sources](https://gitlab.com/opensource-auke/mqtt2discord/-/tree/main)

[Issues](https://gitlab.com/opensource-auke/mqtt2discord/-/issues)

[Docker container on GitLab](https://gitlab.com/opensource-auke/mqtt2discord/container_registry/3058173)

[Docker container on Docker Hub](https://hub.docker.com/repository/docker/aukevanderheide/mqtt2discord)

## Setup

Before you can use this, you have to create a *config.json* file in the *src/* folder.

For this you can copy the *config.json.sample* to *config.json* and edit the file.

## Edit config.json

### host

A string containing the hostname or ip number of the *mqtt* server.

### port

Port number used by the mqtt server. Usually 1883.

### username

User name for the *mqtt* server. This is optional, depending on your *mqtt* server configuration.

### password

Only required if username is set.

### timezone
> from [version 1.0.1](%v1.0.1)

Optional timezone. This is used for the time in the heartbeat messages.

```json
  "timezone": "Europe/Amsterdam",
```

### heartbeat

> from [version 1.0.0](%v1.0.0)

Optional heartbeat for health-check can be enabled in the *config.json* file.

```json
  "heartbeat": {
      "topic": "stats/mqtt2sonoff/heartbeat",
      "minutes": 5
  }
```

#### topic

The topic where the message should be send to

#### minutes

The time between 2 heartbeats in minutes.

#### Output

Returns a json object

```json
{
    "Time": "2022-05-20T16:03:43",
    "Name": "mqtt2discord",
    "Version": "1.0.0",
    "Active": True,
    "Send": 16,
    "Received": 13
}
```
##### Time

The timestamp the message was send.

##### Name

Name of the program.

##### Version

Running version of the program.

##### Active

True if still alive.

##### Send

Total messages send to Discord since last heartbeat.

##### Received

Total messages received from mqtt server since last heartbeat.

### topics

For each topic you want to listen to, there should be an entry in topics. You can add as many topics as you like.

If you want to send multiple topics to the same channel, just add another topic with the same webhook as the first topic.

If you want the same topic on multiple channels, just add another topic with the same topic.

#### topic

The *mqtt* topic to listen to.

#### webhook

The webhook from the channel you want to send the message to.

#### startup_message

If set to **true** a message is send to this *discord* channel on startup of the program.

If set to **false** no startup message will be send to this *discord* channel.

## Use

### Bare metal

You must have **python3** installed.

Install the requirements for the app

```bash
cd src
pip3 install -r requirements.txt
```

Run the app
```bash
python3 main.py
```
### Docker run

```bash
docker run --rm \
  -it \
  --name $app \
  -v "$PWD/src/config.json":/usr/src/bot/config.json \
  registry.gitlab.com/opensource-auke/mqtt2discord/mqtt2discord:latest
```
### Docker Swarm and Docker compose (preferred!)

```yaml
version: '3.7'

x-defaults-opts:
  &default-opts
  logging:
    options:
      max-size: "1m"

services:

  mqtt2discord:
    <<: *default-opts
    image: registry.gitlab.com/opensource-auke/mqtt2discord/mqtt2discord:latest
    networks:
      - backend
    configs:
      - source: mqtt2discord.conf.v0
        target: /usr/src/bot/config.json
    deploy:
      replicas: 1
      update_config:
        failure_action: rollback

networks:

  backend:
    driver: bridge
    name: backend
    attachable: true

# if config change than name must be changed (.vX)
configs:

  mqtt2discord.conf.v0:
    file: ./src/config.json
```

for Docker-compose

```bash
docker compose up -d
```