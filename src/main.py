#!~/modules/venv/bin/python
#
# mqtt2discord
# (c) Copyright Auke van der Heide, May 2022
# Documentation: https://gitlab.com/opensource-auke/mqtt2discord/-/wikis/home
# Sources: https://gitlab.com/opensource-auke/mqtt2discord/-/tree/main
# Issues: https://gitlab.com/opensource-auke/mqtt2discord/-/issues
# Docker container registry: https://gitlab.com/opensource-auke/mqtt2discord/container_registry/3058173
import json
import logging
import sys
from paho.mqtt import client as mqtt
from discord_webhook import DiscordWebhook
import time
import datetime
from pytz import timezone

from config import Config

APP = 'mqtt2discord'
VERSION = "1.0.3-dev"

# Initialise logging
LOG_LEVEL = logging.INFO
LOG_FORMAT = "%(asctime)s %(levelname)s %(message)s"
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)

config = Config('./config.json')

subscribed = []
watchdog = True
total_received = 0
total_send = 0
heartbeat_MQTT = datetime.datetime.now()

MQTT_WATCHDOG_TOPIC = '$SYS/broker/bytes/'
MQTT_WATCHDOG_WEBHOOK = "https://discord.com/api/webhooks/829760294502793268/_62V_tTav3UheQ2EhnazQV01PgFmFXbPTdjOvzibt4w64JRWBP8YnhOVtEF5HsCUkwve"  # NOQA: E501

def on_connect(client, userdata, flags, rc):
    logging.debug("Connected with result code " + str(rc))
    global total_send

    # Subscribe to watchdog topic
    client.subscribe(MQTT_WATCHDOG_TOPIC + '#')

    # Subscribe to topics
    for topic in config.get('topics'):
        topic_name = config.get_sub(key_name='topic', config_name=topic, required=True)
        topic_webhook = config.get_sub(key_name='webhook', config_name=topic, required=True)
        topic_startup_message = config.get_sub(key_name='startup_message', config_name=topic, required=False)
        logging.info(f'''Subscribing to {topic_name}''')
        client.subscribe(topic_name)
        subscribed.append(topic_name)
        if topic_startup_message:
            url = topic_webhook
            payload = f'''{APP} v:{VERSION} is now listening on MQTT topic {topic_name} on server {config.get('host')}:{config.get('port')}'''  # NOQA: E501
            logging.info(f'''Send startup message to {topic_name}''')
            webhook = DiscordWebhook(url=url, content=payload)
            webhook.execute()
            total_send += 1
            topic['startup_message'] = False


def on_message(client, userdata, msg):
    global total_send, total_received, heartbeat_MQTT
    try:
        payload = str(msg.payload.decode())
        total_received += 1
        if msg.topic in subscribed:
            for topic in config.get('topics'):
                topic_name = config.get_sub(key_name='topic', config_name=topic, required=True)
                if topic_name == msg.topic:
                    url = config.get_sub(key_name='webhook', config_name=topic, required=True)
                    logging.info(f'''Send message from {topic_name}''')
                    webhook = DiscordWebhook(url=url, content=payload)
                    webhook.execute()
                    total_send += 1
        elif msg.topic.__contains__(MQTT_WATCHDOG_TOPIC):
            logging.info(f'Received MQTT heartbeat message: {msg.payload}')
            heartbeat_MQTT = datetime.datetime.now()

    except Exception as e:
        print(e)


def main():
    logging.info(f'''Starting {APP} v:{VERSION}''')

    # Abort if there are no topics!
    if len(config.get(key_name='topics')) == 0:
        logging.critical("No topics are defined")
        sys.exit(1)

    # MQTT
    client = mqtt.Client(APP, clean_session=False)

    hostname = config.get('host')
    hostport = config.get('port')
    username = config.get('username', required=False)
    password = config.get('password', required=False)
    if username:
        client.username_pw_set(username, password)
        logging.info(f'''MQTT connect: {username}@{hostname}:{str(hostport)}''')
    else:
        logging.info(f'''MQTT connect: {hostname}:{str(hostport)}''')

    try:
        client.connect(hostname, hostport, 60)
    except Exception as e:
        logging.critical(str(e))
        sys.exit(1)

    client.on_connect = on_connect
    client.on_message = on_message


    client.loop_start()
    global total_send, total_received, heartbeat_MQTT

    while watchdog:

        # check MQTT server
        timestamp = datetime.datetime.now()
        print(f'Timestamp div: {timestamp - heartbeat_MQTT}')
        mqtt_server_offline = False
        if (timestamp - heartbeat_MQTT) > datetime.timedelta(minutes=1):
            logging.warning("MQTT server is down")
            payload = f'MQTT server is off-line!\nhttp://{hostname}:{hostport}'
            webhook = DiscordWebhook(url=MQTT_WATCHDOG_WEBHOOK, content=payload)
            webhook.execute()
            mqtt_server_offline = True
        else:
            logging.warning("MQTT server is up")
            if mqtt_server_offline:
              payload = f'MQTT server is back online!\nhttp://{hostname}:{hostport}'
              webhook = DiscordWebhook(url=MQTT_WATCHDOG_WEBHOOK, content=payload)
              mqtt_server_offline = False
              webhook.execute()


        # check devices heartbeat
        heartbeat = config.get('heartbeat', required=False)
        if heartbeat:
            tz = config.get('timezone', required=False)
            if tz:
                now = datetime.datetime.now(timezone(tz))
            else:
                now = datetime.datetime.now()
            format_date = now.strftime("%Y-%m-%dT%H:%M:%S")
            payload = {
                "Time": format_date,
                "Name": APP,
                "Version": VERSION,
                "Active": True,
                "Send": total_send,
                "Received": total_received
            }
            topic = config.get_sub(key_name='topic', config_name=heartbeat, required=True)
            minutes = config.get_sub(key_name='minutes', config_name=heartbeat, required=True)
            client.publish(topic=topic, payload=json.dumps(payload))
            total_send = 0
            total_received = 0
            time.sleep(60 * minutes)

    client.loop_stop()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:

        print("")
        print("Terminated by user")
        watchdog = False
        sys.exit(0)