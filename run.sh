#!/bin/bash
# run docker container

app=mqtt2discord
version=1.0.3-dev

docker run --rm \
  -it \
  --name $app \
  -v "$PWD/src/config.json":/usr/src/bot/config.json \
  registry.gitlab.com/opensource-auke/mqtt2discord/$app:$version